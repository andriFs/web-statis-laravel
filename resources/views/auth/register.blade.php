<!DOCTYPE html>
<html>
  <head>
    <title>Form</title>
  </head>
  <body>
    <h1>Buat Account Baru</h1>
    <h2>Sign Up Form</h2>
    <form action="register" method="POST">
        @csrf
      <label>First Name :</label> <br />
      <input type="text" name="first_name" required /><br /><br />

      <label>Last Name :</label><br />
      <input type="text" name="last_name" required /> <br /><br />

      <label>Gender :</label><br />
      <input type="radio" name="gender" value="Man" required />Man <br />
      <input type="radio" name="gender" value="Woman" required />Woman <br />
      <input type="radio" name="gender" value="Other" required />Other <br /><br />

      <label>Nationality : </label>
      <select name="nationality" required>
        <option value="indonesia">Indonesia</option>
        <option value="Amerika">Amerika</option>
        <option value="India">India</option>
      </select>
      <br /><br />

      <label>Language Spoken :</label><br />
      <input type="checkbox" name="language[]" value="Bahasa Indonesia" />Bahasa Indonesia <br />
      <input type="checkbox" name="language[]" value="English"/>English <br />
      <input type="checkbox" name="language[]" value="Arabic"/>Arabic<br />
      <input type="checkbox" name="language[]" value="Japanese"/>Japanese<br /><br />

      <label>Bio</label><br /><br />
      <textarea name="bio" id="" cols="30" rows="10"></textarea><br /><br />

      <input type="submit" value="Sign Up" />
    </form>
  </body>
</html>
